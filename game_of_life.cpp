#include "game_of_life.h"

Grid::Grid(char delim, int x, int y)
    : m_delim(delim), m_i(x), m_j(y)
{
    m_front_layer = new bool*[m_i];
    m_back_layer = new bool*[m_i];
    for (int i=0; i<m_i ;i++) {
        m_front_layer[i] = new bool[m_j];
        m_back_layer[i] = new bool[m_j];
        for (int j=0 ; j<m_j; j++) {
            m_front_layer[i][j] = false;
            m_back_layer[i][j] = false;
        }
    }
    srand(1000000);
} 

Grid::~Grid() {
    for (int i=0; i<m_i ;i++) {
            delete[] m_front_layer[i];
            delete[] m_back_layer[i];
    }
    delete[] m_front_layer;
    delete[] m_back_layer;
}

int Grid::number_of_neighbours(int x, int y) {
    int neighbours = 0;
    int start_i = (x == 0 ? 0 : x-1);
    int start_j = (y == 0 ? 0 : y-1);
    int end_i = (x == m_i-1 ? x : x+1);
    int end_j = (y == m_j-1 ? y : y+1);
    for (int i = start_i; i <= end_i; i++){
        for (int j = start_j; j <= end_j; j++){
            neighbours+=m_front_layer[i][j];
        }
    }
    return m_front_layer[x][y] ? neighbours-1 : neighbours;
}

void Grid::populate() {
    // moving camp
//    m_front_layer[m_i/2+1][m_j/2-1] = true;
//    m_front_layer[m_i/2][m_j/2] = true;
//    m_front_layer[m_i/2-2][m_j/2-2] = true;
//    m_front_layer[m_i/2-2][m_j/2-3] = true;
//    m_front_layer[m_i/2][m_j/2-1] = true;
//    m_front_layer[m_i/2-1][m_j/2-1] = true;


    for (int i = 0; i < m_i; i++){
        for (int j = 0; j <= m_j; j++){
            if ((i+j)%2 != 1) {
//            if (rand()%2!=0) {
                m_front_layer[i][j] = true;
            }
        }
    }
}

bool Grid::has_alive() {
    for (int i=0; i<m_i; i++) {
        for (int j=0; j<m_j; j++) {
            if (m_front_layer[i][j]) {
                return true;
            }
        }
    }
    return false;
}

bool Grid::apply_rules() {
    int neighbours = 0;
    bool changed = false;
    for (int i=0; i<m_i; i++){
        for (int j=0; j<m_j; j++){
            m_back_layer[i][j] = m_front_layer[i][j];
            neighbours = number_of_neighbours(i,j); 
            if (m_front_layer[i][j]) {
                if (neighbours < 2 || neighbours > 3) {
                    m_back_layer[i][j] = false;
                    changed = true;
                }
            }
            else if (neighbours == 3) {
                m_back_layer[i][j] = true;
                changed = true;
            }
        }
    }
    std::swap(m_front_layer, m_back_layer);
    return changed;
}

void Grid::print() {
    std::system("clear");
    for (int i=0; i<m_i; i++){
        printf("%c", m_delim);
        for (int j=0; j<m_j; j++){
            if (m_front_layer[i][j])
                printf("*%c", m_delim);
            else 
                printf(" %c", m_delim);
        }
        printf("\n");
    }
    printf("\n");
    std::this_thread::sleep_for(std::chrono::microseconds(100000));
}

void Grid::run() {
    print();
    printf("Press ENTER to start\n");
    getchar();
    while (has_alive() && apply_rules()) {
        print();
    }
    printf("Game of life ended\n");
}
