#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <chrono>
#include <thread>

class Grid {

private:
    bool ** m_front_layer;
    bool ** m_back_layer;
    int m_i;
    int m_j;
    char m_delim;

public:
    // full screen for smallers characters
    // Grid(char delim = ' ', int x = 65*1.5, int y = 118*2.69);
    Grid(char delim = ' ', int x = 65, int y = 118);

    ~Grid();

    int number_of_neighbours(int x, int y);

    void populate();
        
    bool has_alive();

    bool apply_rules();

    void print();
    
    void run();
};
