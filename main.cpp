#include "game_of_life.h"

int main() {

    Grid grid;
    grid.populate();
    grid.run();

}
